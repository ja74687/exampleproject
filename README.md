# ExampleProject

## Service:
Usługa, której zadaniem jest pobranie plików z danej lokalizacji, przeczytanie ich oraz dodanie do bazy danych. Czytane są pliki w formacie XML, i w przypadku poprawnego przetworzenia pliku jest on przenoszony do folderu wskazanego w konfiguracji, natomiast plik który jest niepoprawny przenoszony jest również do odpowiedniego folderu.

## REST_API

Przykład serwisu REST API, który pobiera dane z MSSQL, następnie przy pomocy formatu JSON  wysyła je do danego klienta.

## Frontend

Przykładowa aplikacja napisana w frameworku Angular, wyświetla ona pobrane dane przez REST API. Dane są wyświetlane w formie tabeli, w której mamy możliwość przy pomocy komponentu Dialog wyświetlić szczegóły danego produktu. SPA w przekładzie została napisana tak aby pokazać lazyloading w routingu.

## MSQQL

Zawiera backup pełny bazy, oraz skrypty tabel i procedur użytych w projecie.