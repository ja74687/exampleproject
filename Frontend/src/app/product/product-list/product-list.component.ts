import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, startWith, switchMap, map, catchError, of as observableOf, Subject, debounceTime } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductService } from 'src/app/services/ProductService/product.service';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DetailDialogComponent } from './detail-dialog/detail-dialog.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements AfterViewInit {

  /**
  * Tabela
  */
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns: string[] = ['ProductCategoryName', 'ProductBrandName', 'ProductName', 'Price', 'Stock', 'Description', 'action'];
  data: any;
  refreshTable = new Subject();

  /**
   * Spinner bar
   */
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  SearchForm = new FormGroup({
    SearchValue: new FormControl()
  });

  constructor(
    private productService: ProductService,
    public dialog: MatDialog
  ) { }

  ngAfterViewInit(): void {

    this.SearchForm.valueChanges.pipe(
      debounceTime(1000)
    ).subscribe(() => {
      this.refreshTable.next('');
    })

    merge(this.paginator.page, this.refreshTable)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.GetCarrierList(this.paginator.pageSize, this.paginator.pageIndex, this.SearchForm.value.SearchValue);
        }),
        map(data => {
          // Infoacja o zakończeniu ładowania danych.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data?.length > 0 ? Math.max.apply(Math, data.map(function (o: any) { return o.ElementCount; })) : 0
          return data;
        }),
        catchError(error => {
          this.isLoadingResults = false;
          // Wyłapanie błedu z API
          this.isRateLimitReached = true;
          console.log(error)
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);

  }


  GetCarrierList(sizePage?: number, page?: number, SearchValue?: string) {
    return this.productService.ProductListGet({ PageSize: sizePage, PageIndex: page, SearchValue: SearchValue }); // { SortColumn: sortColumn, SortStyle: sortStyle, PageSize: sizePage, PageIndex: page }
  }

  OpenDetailDailog(item: any) {
    this.dialog.open(DetailDialogComponent, {
      data: {
        ProductName: item.ProductName,
        ProductPhotoURL: item.PhotoURL,
        ProductDescription: item.Description,
        ProductPrice: item.Price
      },
    });
  }

}
