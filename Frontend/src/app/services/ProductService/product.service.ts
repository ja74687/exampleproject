import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient
  ) { }

  ProductListGet(Params: any = {}): Observable<any>{
    return this.http.post<any>(`https://localhost:44336/api/Product/ProductListGet`, Params)
  }
}
