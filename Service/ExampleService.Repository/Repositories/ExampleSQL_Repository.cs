﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ExampleService.Repository.Repositories
{
    public class ExampleSQL_Repository
    {
        private static ExampleSQL_Repository _ExampleSQL_Repository;

        public static ExampleSQL_Repository repository
        {
            get
            {
                if (_ExampleSQL_Repository == null)
                {
                    _ExampleSQL_Repository = new ExampleSQL_Repository();
                }
                return _ExampleSQL_Repository;
            }
        }


        public async Task<(bool error, string message)> setFileData(string XMLData)
        {
            try
            {
                using (var connection = new SqlConnection(@"Server=localhost\SQLEXPRESS;Database=ExampleDataBase;Trusted_Connection=True;"))
                {

                    DynamicParameters dynamicParameters = new DynamicParameters();


                    dynamicParameters.Add("XMLFileData", XMLData);


                    var query = await connection.QueryAsync("[dbo].[SaveFileData]", dynamicParameters, commandType: CommandType.StoredProcedure);

                    string value = JsonConvert.SerializeObject(query);


                    return await Task.FromResult<(bool error, string message)>(((Func<(bool error, string message)>)(() =>
                    {
                        return (false, value);

                    }))());
                }

            }
            catch (Exception ex)
            {
                return await Task.FromResult<(bool error, string message)>(((Func<(bool error, string message)>)(() =>
                {
                    return (true, ex.Message);

                }))());
            }
        }
    }
}
