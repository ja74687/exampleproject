﻿using ExampleService.Contracts.LogManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Flurl.Http;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using ExampleService.Repository.Repositories;

namespace ExampleService
{
    class SeviceMainClass : ServiceBase
    {
        Timer timer;
        string DirectoryFile = "";
        string ArchiveDirectory = "";
        string ErrorDirectory = "";
        List<(string FileName, string FilePath, string FileData)> fileList = new List<(string FileName, string FilePath, string FileData)>();

#if DEBUG
        public SeviceMainClass()
        {
            DirectoryFile = ConfigurationManager.AppSettings.GetValues("DirectoryFile")[0];
            ErrorDirectory = ConfigurationManager.AppSettings.GetValues("ErrorDirectory")[0];

            LogManager.Log.Write("Starting, Service: ");
            TimerTask(null);
        }
#else

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            DirectoryFile = ConfigurationManager.AppSettings.GetValues("DirectoryFile")[0];
            ErrorDirectory = ConfigurationManager.AppSettings.GetValues("ErrorDirectory")[0];

            LogManager.Log.Write("Start usługi");

            timer = new Timer(
                    callback: new TimerCallback(TimerTask),
                    state: null,
                    dueTime: 1000,
                    period: Convert.ToInt32(ConfigurationManager.AppSettings.GetValues("Time")[0])
                );

        }
#endif

        private void TimerTask(object timerState)
        {
            var dataPath = DateTime.Now;
            ArchiveDirectory = ConfigurationManager.AppSettings.GetValues("ArchiveDirectory")[0] + $@"\{dataPath.Year.ToString()}\{dataPath.Month.ToString()}\";

            // Rozpoczęcie wyszukiwania plików.
            SearchFiles(DirectoryFile);


            foreach (var item in fileList)
            {
                Console.WriteLine("Nazwa Pliku:" + item.FileName);
                Console.WriteLine("Ścieżka Pliku:" + item.FilePath);
                Console.WriteLine("Zawartość Pliku:" + item.FileData);
                Console.WriteLine("---------------------------------------------------------");

                var resultSQL = ExampleSQL_Repository.repository.setFileData(item.FileData);

                if (!resultSQL.Result.error)
                {
                    try
                    {
                        if (!Directory.Exists(ArchiveDirectory))
                        {
                            Directory.CreateDirectory(ArchiveDirectory);
                        }
                        File.Move(item.FilePath, ArchiveDirectory + Guid.NewGuid().ToString().Substring(0, 6) + item.FileName);


                    }
                    catch (Exception ex)
                    {
                        LogManager.Log.Write("Błąd podczas przenoszenia plików do archiwum: " + ex.Message);
                    }
                }
              
            }
        }


        protected override void OnStop()
        {
            LogManager.Log.Write("Stopping");
            base.OnStop();
        }

        protected override void OnPause()
        {
            LogManager.Log.Write("Pausing");
            base.OnPause();
        }


        void SearchFiles(string dir)
        {
            try
            {
                fileList = new List<(string FileName, string FilePath, string FileData)>();
                var directory = new DirectoryInfo(dir);
                var dirs = directory.GetDirectories(); //.Where(dire => dire.LastWriteTime > filterDate); ;
                DateTime dt = Directory.GetLastWriteTime(dir);
                GetFiles(dir);


                if (dirs.Count() > 0)
                {
                    foreach (var item in dirs)
                    {
                        if (item.Name != "Archive" && item.Name != "Error")
                        {
                            SearchFiles(item.ToString());
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogManager.Log.Write("Błąd podczas przeszukiwania plików: " + ex.Message);
            }


        }

        void GetFiles(string dir)
        {

            var allowedExtensions = new[] { ".xml" };

            var directory = new DirectoryInfo(dir);
            var fileEntries = directory.GetFiles().OrderBy(x => x.LastWriteTime).Take(20).Where(file => allowedExtensions.Any(file.Name.ToLower().EndsWith));


            // Czytanie listy plików z danej ścieżki
            foreach (var item in fileEntries)
            {
                try
                {

                    var source = File
                                 .ReadLines(item.FullName, System.Text.Encoding.UTF8)
                                 .ToList();
                    // Czytanie zawartości pliku
                    string val = "";
                    for (int i = 0; i < source.Count; i++)
                    {

                        val += source[i].Trim();

                    }

                    XDocument xdoc = XDocument.Parse(val);
                    xdoc.Declaration = null;

                    fileList.Add((item.Name, item.FullName, xdoc.ToString()));
                }
                catch (System.Xml.XmlException e)
                {
                    MoveErrorFile(item);
                    LogManager.Log.Write("Błąd podczas parsowania zawartści pliku: " + item.FullName + " do formatu XML, Błąd: " + e.Message);
                }
                catch (Exception ex)
                {
                    MoveErrorFile(item);
                    LogManager.Log.Write("Błąd podczas czyatania pliku: " + item.FullName + ", Błąd: " + ex.Message);

                }
            }
        }


        void MoveErrorFile(FileInfo item)
        {
            try
            {
                if (!Directory.Exists(ErrorDirectory))
                {
                    Directory.CreateDirectory(ErrorDirectory);
                }
                File.Move(item.FullName, ErrorDirectory + "\\" + Guid.NewGuid().ToString().Substring(0, 6) + item.Name);


            }
            catch (Exception ex)
            {
                LogManager.Log.Write("Błąd podczas przenoszenia plików do folderu błędów: " + ex.Message);
            }
        }

    }
}
