﻿using System;
using System.ServiceProcess;

namespace ExampleService
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            new SeviceMainClass();
#else
            ServiceBase.Run(new SeviceMainClass());
#endif
        }
    }
}
