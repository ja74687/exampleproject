﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExampleService.Contracts.LogManager
{
    public class LogManager
    {
        private static LogManager _logManager;

        public static LogManager Log
        {
            get
            {
                if (_logManager == null)
                {
                    _logManager = new LogManager();
                }
                return _logManager;
            }
        }

        public void Write(string logMessage)
        {
#if DEBUG
            try
            {
                var directoryString = @$"C:\temp\{DateTime.Now.Month.ToString()}_{DateTime.Now.Year.ToString()}" + ".txt";
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(directoryString));
                File.AppendAllText(directoryString, DateTime.Now.ToString() + " ; " + logMessage + Environment.NewLine);
            }
            catch
            {

            }
#else
            try
            {
                var directoryString = @$"C:\temp\{DateTime.Now.Month.ToString()}_{DateTime.Now.Year.ToString()}" + ".txt";
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(directoryString));
                File.AppendAllText(directoryString, DateTime.Now.ToString() + " ; " + logMessage + Environment.NewLine);
            }
            catch
            {

            }
#endif
        }
    }
}
