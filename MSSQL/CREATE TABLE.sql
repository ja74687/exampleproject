CREATE TABLE ProductBrand(
	ID_ProductBrand INT IDENTITY(1,1) PRimary Key,
	ProductBrandName nvarchar(200) NOT NULL,
	CreateDate dateTime default GETDATE()
)

CREATE TABLE ProductCategory(
	ID_ProductCategory INT IDENTITY(1,1) PRimary Key,
	ProductCategoryName nvarchar(200) NOT NULL,
	CreateDate dateTime default GETDATE()
)


CREATE TABLE Product(
	ID_Product int identity(1,1) Primary Key,
	ID_ProductBrand int foreign key references ProductBrand(ID_ProductBrand),
	ID_ProductCategory int foreign key references ProductCategory(ID_ProductCategory),
	ProductName nvarchar(200) NOT NULL,
	CreateDate datetime default GETDATE()
)

CREATE TABLE ProductDetails(
	ID_ProductDetails int identity(1,1) primary key,
	ID_Product int foreign key references Product(ID_Product),
	Price float,
	Stock int,
	[Description] nvarchar(1000),
	PhotoURL nvarchar(500)
)