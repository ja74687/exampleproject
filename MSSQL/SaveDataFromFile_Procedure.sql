USE [ExampleDataBase]
GO
/****** Object:  StoredProcedure [dbo].[SaveFileData]    Script Date: 22.10.2022 15:47:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SaveFileData] 
@XMLFileData nvarchar(MAX)
AS
BEGIN 

	BEGIN TRANSACTION [SaveFileDataTran]

	  BEGIN TRY

			DECLARE @idoc INT

			-- Deklaracja zmiennej tabelarycznej
			DECLARE @TMPTable table (
				id int IDENTITY(1,1),
				Brand NVARCHAR(200),
				ProductName NVARCHAR(500),
				ProductDescription NVARCHAR(1000),
				ProductPrice float,
				ProductCategory NVARCHAR(200),
				ProductStock int,
				ProductPhotoURL NVARCHAR(200)
			)


	
			EXEC sp_xml_preparedocument @idoc OUTPUT, @XMLFileData;  

			-- Dodanie danych z XML do zmiennej tabelarycznej
			INSERT INTO @TMPTable (
				Brand,
				ProductName,
				ProductDescription,
				ProductPrice,
				ProductCategory,
				ProductStock,
				ProductPhotoURL
			)
			SELECT    *  
			FROM       OPENXML (@idoc, '/root/products',2)  
						WITH (
						[brand]  NVARCHAR(200),
						[title]  NVARCHAR(500),
						[description]  NVARCHAR(1000),
						[price]  float,
						[category]  NVARCHAR(200),
						[stock] int,
						[thumbnail]  NVARCHAR(200)
						);

			EXEC sp_xml_removedocument @idoc;

			-- Dodanie kategorii która nie istnieje jeszcze w bazie danych
			INSERT INTO [dbo].[ProductCategory]
				   ([ProductCategoryName])
			SELECT
					ProductCategory 
			FROM @TMPTable AS TMPTa
			LEFT JOIN ProductCategory AS  PrCA ON UPPER(LTRIM(RTRIM(TMPTa.ProductCategory))) = UPPER(LTRIM(RTRIM(PrCa.ProductCategoryName))) 
			WHERE PrCa.ID_ProductCategory IS NULL
			GROUP BY TMPTa.ProductCategory


			-- Dodanie marek urządzeń, które jeszcze nie istnieją w bazie danych
			INSERT INTO [dbo].[ProductBrand]
				   ([ProductBrandName])
			 SELECT
				Brand
			 FROM @TMPTable AS TMPTa
			 LEFT JOIN [ProductBrand] AS  PrBr ON UPPER(LTRIM(RTRIM(TMPTa.Brand))) = UPPER(LTRIM(RTRIM(PrBr.[ProductBrandName]))) 
			 WHERE PrBr.[ID_ProductBrand] IS NULL
			 GROUP BY Brand


			 -- Dodanie produktów, które jeszcze nie istnieją w bazie danych
			 INSERT INTO [dbo].[Product]
				   ([ID_ProductBrand]
				   ,[ID_ProductCategory]
				   ,[ProductName])
			 SELECT
				PrBr.ID_ProductBrand,
				PrCa.ID_ProductCategory,
				TMPTa.ProductName
			 FROM @TMPTable AS TMPTa
			 JOIN [ProductBrand] AS  PrBr ON UPPER(LTRIM(RTRIM(TMPTa.Brand))) = UPPER(LTRIM(RTRIM(PrBr.[ProductBrandName])))
			 JOIN ProductCategory AS  PrCA ON UPPER(LTRIM(RTRIM(TMPTa.ProductCategory))) = UPPER(LTRIM(RTRIM(PrCa.ProductCategoryName))) 
			 LEFT JOIN [Product] AS  Pr ON UPPER(LTRIM(RTRIM(TMPTa.ProductName))) = UPPER(LTRIM(RTRIM(Pr.[ProductName])))
			 WHERE Pr.ID_Product IS NULL
			 GROUP BY TMPTa.ProductName, PrBr.ID_ProductBrand, PrCa.ID_ProductCategory

			 -- Dodanie informacji o danym produkcje, w przypadku kiedy produkt zostanie znaleziony
			 -- Dane o nim zostaną zaktualizowane
			 -- Jeżeli nie ma takich informacji dostaną one dodane.
			MERGE ProductDetails AS PrDe
			USING (
					 SELECT
					Pr.ID_Product,
					TMPTa.ProductPrice,
					TMPTa.ProductStock,
					TMPTa.ProductDescription,
					TMPTa.ProductPhotoURL
				 FROM @TMPTable AS TMPTa
				 JOIN [ProductBrand] AS  PrBr ON UPPER(LTRIM(RTRIM(TMPTa.Brand))) = UPPER(LTRIM(RTRIM(PrBr.[ProductBrandName])))
				 JOIN ProductCategory AS  PrCA ON UPPER(LTRIM(RTRIM(TMPTa.ProductCategory))) = UPPER(LTRIM(RTRIM(PrCa.ProductCategoryName))) 
				 JOIN [Product] AS  Pr ON UPPER(LTRIM(RTRIM(TMPTa.ProductName))) = UPPER(LTRIM(RTRIM(Pr.[ProductName])))
				 GROUP BY Pr.ID_Product,TMPTa.ProductPrice, TMPTa.ProductStock, TMPTa.ProductDescription, TMPTa.ProductPhotoURL
				) AS PrTMP
			ON  UPPER(LTRIM(RTRIM(PrDe.ID_Product))) = UPPER(LTRIM(RTRIM(PrTMP.[ID_Product]))) AND
				UPPER(LTRIM(RTRIM(PrDe.Price))) = UPPER(LTRIM(RTRIM(PrTMP.ProductPrice))) AND
				UPPER(LTRIM(RTRIM(PrDe.Stock))) = UPPER(LTRIM(RTRIM(PrTMP.ProductStock))) AND
				UPPER(LTRIM(RTRIM(PrDe.[Description]))) = UPPER(LTRIM(RTRIM(PrTMP.ProductDescription))) AND
				UPPER(LTRIM(RTRIM(PrDe.PhotoURL))) = UPPER(LTRIM(RTRIM(PrTMP.ProductPhotoURL)))
			WHEN NOT MATCHED THEN
				INSERT ([ID_Product], [Price], [Stock], [Description], [PhotoURL])
				VALUES(PrTMP.ID_Product, PrTMP.ProductPrice, PrTMP.ProductStock, PrTMP.ProductDescription, PrTMP.ProductPhotoURL)
			WHEN MATCHED THEN UPDATE
				SET 
					[Price] = PrTMP.ProductPrice,
					[Stock] = PrTMP.ProductStock,
					[Description] = PrTMP.ProductDescription,
					[PhotoURL]= PrTMP.ProductPhotoURL;

		  COMMIT TRANSACTION [SaveFileDataTran]

	  END TRY

	  BEGIN CATCH

		  ROLLBACK TRANSACTION [SaveFileDataTran]

	  END CATCH 
END