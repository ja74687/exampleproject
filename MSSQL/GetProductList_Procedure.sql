USE [ExampleDataBase]
GO
/****** Object:  StoredProcedure [dbo].[ProductList_GET]    Script Date: 23.10.2022 13:19:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ProductList_GET]
@SearchValue nvarchar(100) = null,
@PageSize int = null,
@PageIndex int = null
AS
BEGIN

	SELECT 
		   COUNT(ID_ProductDetails) OVER () AS 'ElementCount',
		   [ID_ProductDetails]
		  ,PrDe.[ID_Product]
		  ,Pr.ProductName
		  ,PrBr.ID_ProductBrand
		  ,PrBr.ProductBrandName
		  ,PrCa.ID_ProductCategory
		  ,PrCa.ProductCategoryName
		  ,[Price]
		  ,[Stock]
		  ,[Description]
		  ,[PhotoURL]
	  FROM [ExampleDataBase].[dbo].[ProductDetails] AS PrDe
	  JOIN Product AS Pr ON PrDe.ID_Product = Pr.ID_Product
	  JOIN ProductCategory AS PrCa ON Pr.ID_ProductCategory = PrCa.ID_ProductCategory
	  JOIN ProductBrand AS PrBr ON Pr.ID_ProductBrand = PrBr.ID_ProductBrand
	  WHERE ProductName LIKE '%'+@SearchValue+'%' OR @SearchValue IS NULL
	  ORDER BY PrBr.ID_ProductBrand, PrCa.ProductCategoryName 
	  OFFSET(@PageSize * @PageIndex) ROWS FETCH NEXT @PageSize ROWS ONLY

END