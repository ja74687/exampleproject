﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace REST_API_Example.Contracts.ConfigManager
{
    public class ConfigManager
    {
        #region ZMIENNE

        private static ConfigManager _configManager;
        private IConfiguration configuration { get; }

        #endregion

        #region Konstruktor i LOCK

        public static ConfigManager Config
        {
            get
            {
                if (_configManager == null)
                {
                    _configManager = new ConfigManager();
                }
                return _configManager;
            }
        }

        public ConfigManager()
        {
            // Kompilacja pliku ustawień
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            configuration = builder.Build();
        }

        #endregion

        #region ConnectionString
        /// <summary>
        /// Pobranie z konfiguracji ConnectionString
        /// </summary>
        /// <returns></returns>
        public string ConnectionStringExampleDataBase_GET()
        {
            return configuration.GetConnectionString("ExampleDataBase");
        }
        #endregion
    }
}
