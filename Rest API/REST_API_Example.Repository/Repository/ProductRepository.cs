﻿using Dapper;
using Newtonsoft.Json;
using REST_API_Example.Contracts.ConfigManager;
using REST_API_Example.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace REST_API_Example.Repository.Repository
{
    public class ProductRepository : IProductRepository
    {
        public async Task<(bool isError, string dataFromSQL)> ProductListGET(object paramObj)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigManager.Config.ConnectionStringExampleDataBase_GET()))
                {
                    DynamicParameters dynamicParameters = new DynamicParameters();
                    if (paramObj != null)
                    {
                        var param = JsonConvert.DeserializeObject<IDictionary<string, object>>(paramObj.ToString());
                        foreach (var item in param)
                        {
                            dynamicParameters.Add(item.Key, item.Value);
                        }
                    }


                    var query = await connection.QueryAsync("dbo.ProductList_GET", dynamicParameters, commandType: CommandType.StoredProcedure);

                    string value = JsonConvert.SerializeObject(query);


                    return await Task.FromResult<(bool error, string message)>(((Func<(bool error, string message)>)(() =>
                    {
                        return (false, value);

                    }))());
                }

            }
            catch (Exception ex)
            {
                return await Task.FromResult<(bool error, string message)>(((Func<(bool error, string message)>)(() =>
                {
                    return (true, ex.Message);

                }))());
            }
        }
    }
}
