﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace REST_API_Example.Repository.Interface
{
    public interface IProductRepository
    {
        Task<(bool isError, string dataFromSQL)> ProductListGET(object? paramObj);
    }
}
