﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using REST_API_Example.Repository.Interface;
using System;
using System.Threading.Tasks;

namespace REST_API_Example.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        IProductRepository _productRepository;
        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpPost()]
        public async Task<IActionResult> ProductListGet(object? paramsObj = null)
        {

            var result = await _productRepository.ProductListGET(paramsObj);

            if (!result.isError)
            {
                return await Task.FromResult<OkObjectResult>(((Func<OkObjectResult>)(() =>
                {
                    return Ok(result.dataFromSQL);

                }))());
            }
            else
            {
                return await Task.FromResult<BadRequestObjectResult>(((Func<BadRequestObjectResult>)(() =>
                {
                    return BadRequest(result.dataFromSQL);

                }))());
            }
        }
    }
}
